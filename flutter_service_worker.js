'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "version.json": "06dc65ea7e727d2292b431c6cbaa0939",
"index.html": "3ebe2de0307350e90d5b4e3a7ace9ce3",
"/": "3ebe2de0307350e90d5b4e3a7ace9ce3",
"main.dart.js": "611618ae87e1f267b70eb28f3c30af20",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"manifest.json": "1746c1d5dd371ce75b2ee92bb4efa90f",
"assets/AssetManifest.json": "0dc4c411834e317bdae6130ae01da9d5",
"assets/NOTICES": "e65db4c9bf5d87c2ff46eaab9a731d85",
"assets/FontManifest.json": "7b2a36307916a9721811788013e65289",
"assets/fonts/MaterialIcons-Regular.otf": "4e6447691c9509f7acdbf8a931a85ca1",
"assets/assets/sideMenu_icons/user.svg": "4339ba0288b8680b7ec572bff86988b8",
"assets/assets/sideMenu_icons/account.svg": "4027c1e8e67b56a931f238c1b65b5c6d",
"assets/assets/sideMenu_icons/reconciliation.svg": "67085b7c3365b4d8848aba122090ef8d",
"assets/assets/sideMenu_icons/options-lines.svg": "e2982132a18c635993304af6f6a19a57",
"assets/assets/sideMenu_icons/recycle_bin.svg": "507d995d158742a7435a662e8fb5fd85",
"assets/assets/sideMenu_icons/settings_icon.svg": "8f3869745da09fdda8a57bd244fdafaf",
"assets/assets/sideMenu_icons/chart_of_account.svg": "7c7b3bc2fa9e337a38bc3360cd3c720b",
"assets/assets/sideMenu_icons/transaction_icon": "1f8c1ed31d7765d705b2ec6650b279bc",
"assets/assets/sideMenu_icons/dashboard_icon.svg": "6af3605956f4c2d1149414f755dc5306",
"assets/assets/sideMenu_icons/purchase.svg": "58aee8fbb13b28887ef7d0f1b2267ae1",
"assets/assets/sideMenu_icons/reports.svg": "494db33571d4b2b4c62d99d8b918b9b3",
"assets/assets/sideMenu_icons/chart_account.svg": "07d8e1431dc48e2b4cdfe955007fd10f",
"assets/assets/sideMenu_icons/tax_report.svg": "ab35eff54e36e272a1fd732cfac54181",
"assets/assets/sideMenu_icons/reports_icon.svg": "b252bae0745ab566ea21f2faa829921d",
"assets/assets/sideMenu_icons/delivery_notes.svg": "19fa1ff33b5ab4f214133616a4c961c6",
"assets/assets/sideMenu_icons/transaction.svg": "49cc9da34ff04df01e3dcdd6b9f2ffdd",
"assets/assets/sideMenu_icons/ageing_report.svg": "e6c51497ee65bf7e90bd4bf9f860810a",
"assets/assets/sideMenu_icons/transactions.svg": "1f8c1ed31d7765d705b2ec6650b279bc",
"assets/assets/sideMenu_icons/taxes.svg": "9f9d967643ec4a4cf92936ec01efb249",
"assets/assets/sideMenu_icons/category.svg": "40ed8efbe5916afe3cbce413f9ae1ebe",
"assets/assets/sideMenu_icons/statement_icon.svg": "af48cb7203020e1150ca5ec592f1bff7",
"assets/assets/sideMenu_icons/user-line.svg": "9b1ee833380b79c1572097cad8f16b0f",
"assets/assets/sideMenu_icons/general.svg": "4ce7c02b13ec5048483828fba89a7a9a",
"assets/assets/sideMenu_icons/delivery_note_icon.svg": "f88dff2d6042075181c827e2c5066170",
"assets/assets/sideMenu_icons/purchases.svg": "611da378b9cc95510d9378cee68ca991",
"assets/assets/sideMenu_icons/tax_icon": "9f9d967643ec4a4cf92936ec01efb249",
"assets/assets/sideMenu_icons/recon_icon_arrow.svg": "9e18e3413106b2db19f801f0cac1f917",
"assets/assets/sideMenu_icons/suppliers.svg": "bf16d9f0fc1c9a445aca34162931683c",
"assets/assets/sideMenu_icons/tax.svg": "e36ba7b6b655a560433e453541956ed1",
"assets/assets/sideMenu_icons/statement.svg": "a1e73ce510487490aeda26b3f637771c",
"assets/assets/sideMenu_icons/purchase_icon": "611da378b9cc95510d9378cee68ca991",
"assets/assets/sideMenu_icons/general_icon.svg": "4c2903cee5414909d101489425dfabfa",
"assets/assets/transaction.png": "4789013b1e27afe12e5d85a824c14b53",
"assets/assets/Dockket_logo.png": "fdf18c30df144918754fd238edc32348",
"assets/assets/images/profile_pic.png": "5f56c3070f1c066ae15ecad12fb44f54",
"assets/assets/images/logo.png": "5315be9d0a7602fb12a0ad4c2e3006e9",
"assets/assets/chart_account.svg": "3f84831e6576364729986574eb62c7bf",
"assets/assets/login_image.png": "065a3e825fd80a5bd2a9bef7dacc56ea",
"assets/assets/login_f_page.png": "ca12ebf03be57988a12299fe36d14e96",
"assets/assets/docketname.png": "73565867c4cf4c6b00c86cc409a7c7c8",
"assets/assets/icons/Search.svg": "396d519c18918ed763d741f4ba90243a",
"assets/assets/icons/google_drive.svg": "9a3005a58d47a11bfeffc11ddd3567c1",
"assets/assets/icons/menu_setting.svg": "d0e24d5d0956729e0e2ab09cb4327e32",
"assets/assets/icons/menu_dashbord.svg": "b2cdf62e9ce9ca35f3fc72f1c1fcc7d4",
"assets/assets/icons/doc_file.svg": "552a02a5a3dbaee682de14573f0ca9f3",
"assets/assets/icons/unknown.svg": "b2f3cdc507252d75dea079282f14614f",
"assets/assets/icons/menu_task.svg": "1a02d1c14f49a765da34487d23a3093b",
"assets/assets/icons/menu_profile.svg": "fe56f998a7c1b307809ea3653a1b62f9",
"assets/assets/icons/menu_store.svg": "2fd4ae47fd0fca084e50a600dda008cd",
"assets/assets/icons/sound_file.svg": "fe212d5edfddd0786319edf50601ec73",
"assets/assets/icons/pdf_file.svg": "ca854643eeee7bedba7a1d550e2ba94f",
"assets/assets/icons/drop_box.svg": "3295157e194179743d6093de9f1ff254",
"assets/assets/icons/xd_file.svg": "38913b108e39bcd55988050d2d80194c",
"assets/assets/icons/menu_tran.svg": "6c95fa7ae6679737dc57efd2ccbb0e57",
"assets/assets/icons/media.svg": "059dfe46bd2d92e30bf361c2f7055c3b",
"assets/assets/icons/menu_doc.svg": "09673c2879de2db9646345011dbaa7bb",
"assets/assets/icons/supplier_bg.svg": "1fff2452e8778e5a6ca89f12a4ed9e57",
"assets/assets/icons/Figma_file.svg": "0ae328b79325e7615054aed3147c81f9",
"assets/assets/icons/menu_notification.svg": "460268d6e4bdeab56538d7020cc4b326",
"assets/assets/icons/logo.svg": "b3af0c077a73709c992d7e075b76ce33",
"assets/assets/icons/media_file.svg": "2ac15c968f8a8cea571a0f3e9f238a66",
"assets/assets/icons/folder.svg": "40a82e74e930ac73aa6ccb85d8c5a29c",
"assets/assets/icons/Documents.svg": "51896b51d35e28711cf4bd218bde185d",
"assets/assets/icons/excle_file.svg": "dc91b258ecf87f5731fb2ab9ae15a3ec",
"assets/assets/icons/one_drive.svg": "aa908c0a29eb795606799385cdfc8592",
"assets/assets/doc_logo.png": "c327d1a353442693f5f3033d40ae37eb",
"assets/assets/900px.png": "9c5c1fd973ee22d83110121e8ac69ee9",
"assets/assets/dock_logo.png": "768881385f220fb1c678151896ddd954"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "/",
"main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache.
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
